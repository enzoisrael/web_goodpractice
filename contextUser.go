package main

import (
	"context"
	"gopkg.in/mgo.v2"
)

type contextKey string
type User struct {
	nombre string
	email string
}

var userContextKey contextKey = "user"

func NewUserContext(ctx context.Context, user *User) context.Context {
	return context.WithValue(ctx, userContextKey, user)
}

func UserFromContext(ctx context.Context) (*User, bool) {
	u, ok := ctx.Value(userContextKey).(*User)
	return u, ok
}

func UserMustFromContext(ctx context.Context) *User {
	u, ok := ctx.Value(userContextKey).(*User)
	if !ok {
		panic("user not found in context")
	}
	return u
}

func GetUser(db *mgo.Database, authToken string) (User, error){
	//TODO: busqueda por token
	return User{nombre: "enzo", email: "enzo@curiosity.pe"}, nil
}
